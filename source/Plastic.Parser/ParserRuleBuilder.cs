﻿namespace Plastic.Parser
{
	internal class ParserRuleBuilder<TToken> : IParserRuleBuilder<TToken>, INextParserRuleBuilder<TToken>
	{
		public INextParserRuleBuilder<TToken> Accept(TToken token)
		{
			return this;
		}

		public INextParserRuleBuilder<TToken> Or(TToken token)
		{
			return this;
		}

		public INextParserRuleBuilder<TToken> And(TToken token)
		{
			return this;
		}
	}
}