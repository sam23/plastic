using Plastic.Parser.Core.Stream;
using Plastic.Parser.Lexer;

namespace Plastic.Parser
{
	public interface IGrammar<TToken, out TExpression>
	{
		ILexer<TToken> CreateLexer(IInputStream inputStream);
		IParser<TExpression> CreateParser(IInputStream inputStream);
	}
}