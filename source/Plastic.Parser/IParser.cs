﻿namespace Plastic.Parser
{
	public interface IParser<out TExpression>
	{
		ITree<TExpression> Parse();
	}
}