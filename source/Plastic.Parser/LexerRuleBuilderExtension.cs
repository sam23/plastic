﻿using System;
using Plastic.Parser.Core.Lexer.Rules;
using Plastic.Parser.Core.Lexer.Rules.Verification;
using Plastic.Parser.Lexer;
using Plastic.Parser.Lexer.Verification;

namespace Plastic.Parser
{
	public static class LexerRuleBuilderExtension
	{
		public static void Is(this ILexerRuleBuilder ruleBuilder, string text)
		{
			foreach (var character in text.ToCharArray())
			{
				var verification = new IsCharacterVerification(character) { Occurence = Occurence.Single };
				ruleBuilder.AddVerification(verification);
			}
		}

		public static IVerificationBuilder AcceptDigits(this ILexerRuleBuilder ruleBuilder)
		{
			return ruleBuilder.AddVerification(new IsDigitVerification());
		}

		public static IVerificationBuilder AcceptNumbers(this ILexerRuleBuilder ruleBuilder)
		{
			return ruleBuilder.AddVerification(new IsNumberVerification());
		}

		public static IVerificationBuilder AcceptLetters(this ILexerRuleBuilder ruleBuilder)
		{
			return ruleBuilder.AddVerification(new IsLetterVerification());
		}

		public static IVerificationBuilder AcceptLettersOrDigits(this ILexerRuleBuilder ruleBuilder)
		{
			return ruleBuilder.AddVerification(new IsLetterOrDigitVerification());
		}

		public static IVerificationBuilder Accept(this ILexerRuleBuilder ruleBuilder, char character)
		{
			return ruleBuilder.AddVerification(new IsCharacterVerification(character));
		}

		public static IVerificationBuilder Accept(this ILexerRuleBuilder ruleBuilder, Func<char, bool> verificationDelegate)
		{
			return ruleBuilder.AddVerification(new CharDelegateVerification(verificationDelegate));
		}
	}
}