﻿namespace Plastic.Parser.Lexer
{
	public interface ILexer<T>
	{
		Token<T> GetNextToken();
	}
}