﻿namespace Plastic.Parser.Lexer
{
	public class Token<T>
	{
		public Token(T tokenType, string data)
		{
			TokenType = tokenType;
			Data = data;
		}

		public T TokenType { get; private set; }
		public string Data { get; private set; }
	}
}