namespace Plastic.Parser.Lexer.Verification
{
	public enum Occurence
	{
		ZeroOrMany,
		OneOrMany,
		ZeroOrSingle,
		Single
	}
}