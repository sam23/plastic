namespace Plastic.Parser.Lexer.Verification
{
	public interface IVerification
	{
		Occurence Occurence { get; set; }
		bool Verify(ILexerState state);
	}
}