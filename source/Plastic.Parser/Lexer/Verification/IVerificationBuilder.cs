namespace Plastic.Parser.Lexer.Verification
{
	public interface IVerificationBuilder
	{
		ILexerRuleBuilder ZeroOrMany();
		ILexerRuleBuilder OneOrMany();
		ILexerRuleBuilder ZeroOrSingle();
		ILexerRuleBuilder Single();
	}
}