﻿namespace Plastic.Parser.Lexer
{
	public interface ILexerState
	{
		char LookAhead { get; }
		int Position { get; }
	}
}