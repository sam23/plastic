using Plastic.Parser.Lexer.Verification;

namespace Plastic.Parser.Lexer
{
	public interface ILexerRuleBuilder
	{
		IVerificationBuilder AddVerification(IVerification verification);
	}
}