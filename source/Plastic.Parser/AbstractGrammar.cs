﻿using System.Collections.Generic;
using System.Linq;
using Plastic.Parser.Core;
using Plastic.Parser.Core.Lexer;
using Plastic.Parser.Core.Lexer.Rules;
using Plastic.Parser.Core.Parser;
using Plastic.Parser.Core.Stream;
using Plastic.Parser.Lexer;

namespace Plastic.Parser
{
	public abstract class AbstractGrammar<TToken, TExpression> : IGrammar<TToken, TExpression>
	{
		private readonly ILexerRuleFactory<TToken> _lexerRuleFactory;
		private readonly List<ILexerRule<TToken>> _lexerRules = new List<ILexerRule<TToken>>();

		protected AbstractGrammar() : this(new LexerRuleFactory<TToken>(new IdentifierGenerator()))
		{
		} 

		internal AbstractGrammar(ILexerRuleFactory<TToken> lexerRuleFactory)
		{
			_lexerRuleFactory = lexerRuleFactory;
		}

		public ILexer<TToken> CreateLexer(IInputStream inputStream)
		{
			return new Lexer<TToken>(inputStream, _lexerRules);
		}

		public IParser<TExpression> CreateParser(IInputStream inputStream)
		{
			return new Parser<TToken, TExpression>(CreateLexer(inputStream), Enumerable.Empty<ParserRule<TToken, TExpression>>());
		}

		protected ILexerRuleBuilder Token(TToken tokentType)
		{
			var lexerRule = _lexerRuleFactory.Create(tokentType);

			_lexerRules.Add(lexerRule);

			return new LexerRuleBuilder<TToken>(lexerRule);
		}

		protected IParserRuleBuilder<TToken> Expression(TExpression expressionType)
		{
			return new ParserRuleBuilder<TToken>();
		}
	}
}
