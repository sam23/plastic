﻿namespace Plastic.Parser
{
	public interface IParserRuleBuilder<TToken>
	{
		INextParserRuleBuilder<TToken> Accept(TToken token);
	}
}