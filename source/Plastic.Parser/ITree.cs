﻿namespace Plastic.Parser
{
	public interface ITree<out TExpression>
	{
		TExpression ExpressionType { get; }
		ITree<TExpression> GetChild(int index);
	}
}