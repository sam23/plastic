﻿namespace Plastic.Parser
{
	public interface INextParserRuleBuilder<TToken>
	{
		INextParserRuleBuilder<TToken> Or(TToken token);
		INextParserRuleBuilder<TToken> And(TToken token);
	}
}