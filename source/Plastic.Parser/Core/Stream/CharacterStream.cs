﻿using System;
using System.Linq;

namespace Plastic.Parser.Core.Stream
{
	public class CharacterStream : IInputStream
	{
		private readonly char[] _source;

		public CharacterStream(string source)
		{
			_source = source.ToArray();
		}

		public int Position
		{
			get; private set;
		}

		public char Consume()
		{
			var character = LookAhead(1);
			Position++;

			return character;
		}

		public char LookAhead(int offset)
		{
			var index = Position + offset - 1;

			return index < _source.Length ? _source[index] : Char.MinValue;
		}
	}
}