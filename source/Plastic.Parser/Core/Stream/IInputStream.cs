﻿namespace Plastic.Parser.Core.Stream
{
	public interface IInputStream
	{
		int Position { get; }
		char Consume();
		char LookAhead(int offset);
	}
}