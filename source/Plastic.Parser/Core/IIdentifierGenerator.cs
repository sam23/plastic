﻿using System;

namespace Plastic.Parser.Core
{
	public interface IIdentifierGenerator
	{
		Guid Generate();
	}
}