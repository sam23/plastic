﻿using System.Collections.Generic;
using System.Linq;
using Plastic.Parser.Lexer;

namespace Plastic.Parser.Core.Parser
{
	internal class Parser<TToken, TExpression> : IParser<TExpression>
	{
		private readonly ILexer<TToken> _lexer;
		private readonly IEnumerable<ParserRule<TToken, TExpression>> _parserRules;

		public Parser(ILexer<TToken> lexer, IEnumerable<ParserRule<TToken, TExpression>> parserRules)
		{
			_lexer = lexer;
			_parserRules = parserRules.ToList();
		}

		public ITree<TExpression> Parse()
		{
			var token = _lexer.GetNextToken();

			return new CommonTree<TExpression>(default(TExpression));
		}
	}

	internal class ParserRule<TToken, TExpression>
	{
		public ParserRule(TExpression expressionType)
		{
			ExpressionType = expressionType;
		}

		public TExpression ExpressionType { get; private set; }
	}
}