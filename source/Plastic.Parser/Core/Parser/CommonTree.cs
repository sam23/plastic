﻿using System.Collections.Generic;

namespace Plastic.Parser.Core.Parser
{
	internal class CommonTree<TExpression> : ITree<TExpression>
	{
		private readonly List<CommonTree<TExpression>> _childs = new List<CommonTree<TExpression>>();

		public CommonTree(TExpression expressionType)
		{
			ExpressionType = expressionType;
		}

		public TExpression ExpressionType { get; private set; }

		public ITree<TExpression> GetChild(int index)
		{
			return _childs[0];
		}

		public void AddChild(CommonTree<TExpression> child)
		{
			_childs.Add(child);
		}
	}
}