﻿using Plastic.Parser.Core.Lexer.Rules.Verification;
using Plastic.Parser.Lexer;

namespace Plastic.Parser.Core.Lexer
{
	internal interface ILexerStateInternal : ILexerState, IVerificationRegistry
	{
	}
}