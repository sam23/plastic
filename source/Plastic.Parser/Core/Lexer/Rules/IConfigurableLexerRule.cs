﻿using Plastic.Parser.Core.Lexer.Rules.Verification;

namespace Plastic.Parser.Core.Lexer.Rules
{
	internal interface IConfigurableLexerRule<T> : ILexerRule<T>, IVerificationContainer
	{
	}
}