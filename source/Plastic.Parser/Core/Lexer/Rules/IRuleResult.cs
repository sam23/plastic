﻿namespace Plastic.Parser.Core.Lexer.Rules
{
	internal interface IRuleResult<T>
	{
		ILexerRule<T> Rule { get; } 
		bool Completed { get; }
		bool Success { get; }
	}
}