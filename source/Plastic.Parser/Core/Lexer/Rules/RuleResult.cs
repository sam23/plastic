﻿namespace Plastic.Parser.Core.Lexer.Rules
{
	internal class RuleResult<T> : IRuleResult<T>
	{
		public RuleResult(ILexerRule<T> rule)
		{
			Rule = rule;
		}

		public ILexerRule<T> Rule { get; private set; } 
		public bool Completed { get; set; }
		public bool Success { get; set; }
	}
}