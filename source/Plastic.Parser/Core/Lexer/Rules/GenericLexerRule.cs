﻿using Plastic.Parser.Lexer.Verification;

namespace Plastic.Parser.Core.Lexer.Rules
{
	internal class GenericLexerRule<T> : IConfigurableLexerRule<T>
	{
		private readonly IIdentifierGenerator _identifierGenerator;
		private VerificationChain _verificationChain;
 
		public GenericLexerRule(T tokenType, IIdentifierGenerator identifierGenerator)
		{
			TokenType = tokenType;
			_identifierGenerator = identifierGenerator;
		}

		public T TokenType { get; private set; }

		public IRuleResult<T> Accept(ILexerStateInternal state)
		{
			var verificationResult = _verificationChain != null ? _verificationChain.Verify(state) : new VerificationResult();

			var result = new RuleResult<T>(this)
			{
				Success = verificationResult.Success,
				Completed = verificationResult.Completed
			};

			return result;
		}

		public void AddVerification(IVerification verification)
		{
			if (_verificationChain != null)
			{
				_verificationChain.AddSuccessor(new VerificationChain(verification, _identifierGenerator.Generate()));
			}
			else
			{
				_verificationChain = new VerificationChain(verification, _identifierGenerator.Generate());
			}
		}

		public override string ToString()
		{
			return string.Format("Lexer Rule Token='{0}' Verification={1}", TokenType, _verificationChain);
		}
	}
}