using Plastic.Parser.Core.Lexer.Rules.Verification;
using Plastic.Parser.Lexer;
using Plastic.Parser.Lexer.Verification;

namespace Plastic.Parser.Core.Lexer.Rules
{
	internal class LexerRuleBuilder<T> : ILexerRuleBuilder
	{
		private readonly IConfigurableLexerRule<T> _lexerRule;

		public LexerRuleBuilder(IConfigurableLexerRule<T> lexerRule)
		{
			_lexerRule = lexerRule;
		}

		public IVerificationBuilder AddVerification(IVerification verification)
		{
			_lexerRule.AddVerification(verification);

			return new VerificationBuilder(this, verification);
		}
	}
}