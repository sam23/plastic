﻿using System;
using Plastic.Parser.Lexer;

namespace Plastic.Parser.Core.Lexer.Rules.Verification
{
	internal class CharDelegateVerification : VerificationBase
	{
		private readonly Func<char, bool> _verificationDelegate;

		public CharDelegateVerification(Func<char, bool> verificationDelegate)
		{
			_verificationDelegate = verificationDelegate;
		}

		public override bool Verify(ILexerState state)
		{
			return _verificationDelegate(state.LookAhead);
		}
	}
}