﻿using System;
using Plastic.Parser.Lexer;

namespace Plastic.Parser.Core.Lexer.Rules.Verification
{
	internal class IsLetterVerification : VerificationBase
	{
		public override bool Verify(ILexerState state)
		{
			return Char.IsLetter(state.LookAhead);
		}
	}
}