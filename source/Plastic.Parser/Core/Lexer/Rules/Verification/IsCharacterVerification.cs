﻿using Plastic.Parser.Lexer;

namespace Plastic.Parser.Core.Lexer.Rules.Verification
{
	internal class IsCharacterVerification : VerificationBase
	{
		private readonly char _character;

		public IsCharacterVerification(char character)
		{
			_character = character;
		}

		public override bool Verify(ILexerState state)
		{
			return state.LookAhead == _character;
		}
	}
}