﻿using System;
using Plastic.Parser.Lexer;

namespace Plastic.Parser.Core.Lexer.Rules.Verification
{
	internal class IsDigitVerification : VerificationBase
	{
		public override bool Verify(ILexerState state)
		{
			return Char.IsDigit(state.LookAhead);
		}
	}
}