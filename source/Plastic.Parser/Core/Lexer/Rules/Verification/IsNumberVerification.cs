﻿using System;
using Plastic.Parser.Lexer;

namespace Plastic.Parser.Core.Lexer.Rules.Verification
{
	internal class IsNumberVerification : VerificationBase
	{
		public override bool Verify(ILexerState state)
		{
			return Char.IsNumber(state.LookAhead);
		}
	}
}