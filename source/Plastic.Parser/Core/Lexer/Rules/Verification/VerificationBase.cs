﻿using Plastic.Parser.Lexer;
using Plastic.Parser.Lexer.Verification;

namespace Plastic.Parser.Core.Lexer.Rules.Verification
{
	internal abstract class VerificationBase : IVerification
	{
		public Occurence Occurence
		{
			get; set;
		}

		public abstract bool Verify(ILexerState state);
	}
}