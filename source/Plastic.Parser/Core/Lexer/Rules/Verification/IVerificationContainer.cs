﻿using Plastic.Parser.Lexer.Verification;

namespace Plastic.Parser.Core.Lexer.Rules.Verification
{
	internal interface IVerificationContainer
	{
		void AddVerification(IVerification verification);
	}
}