﻿using System;

namespace Plastic.Parser.Core.Lexer.Rules.Verification
{
	internal interface IVerificationRegistry
	{
		void AddSuccessfulVerification(Guid id);
		bool IsSuccessfulVerification(Guid id);
	}
}