﻿using System;
using Plastic.Parser.Lexer;

namespace Plastic.Parser.Core.Lexer.Rules.Verification
{
	internal class IsLetterOrDigitVerification : VerificationBase
	{
		public override bool Verify(ILexerState state)
		{
			return Char.IsLetterOrDigit(state.LookAhead);
		}
	}
}