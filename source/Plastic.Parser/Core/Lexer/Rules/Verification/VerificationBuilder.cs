using Plastic.Parser.Lexer;
using Plastic.Parser.Lexer.Verification;

namespace Plastic.Parser.Core.Lexer.Rules.Verification
{
	internal class VerificationBuilder : IVerificationBuilder
	{
		private readonly ILexerRuleBuilder _lexerRuleBuilder;
		private readonly IVerification _verification;

		public VerificationBuilder(ILexerRuleBuilder lexerRuleBuilder, IVerification verification)
		{
			_lexerRuleBuilder = lexerRuleBuilder;
			_verification = verification;
		}

		public ILexerRuleBuilder ZeroOrMany()
		{
			_verification.Occurence = Occurence.ZeroOrMany;

			return _lexerRuleBuilder;
		}

		public ILexerRuleBuilder OneOrMany()
		{
			_verification.Occurence = Occurence.OneOrMany;

			return _lexerRuleBuilder;
		}

		public ILexerRuleBuilder ZeroOrSingle()
		{
			_verification.Occurence = Occurence.ZeroOrSingle;

			return _lexerRuleBuilder;
		}

		public ILexerRuleBuilder Single()
		{
			_verification.Occurence = Occurence.Single;

			return _lexerRuleBuilder;
		}
	}
}