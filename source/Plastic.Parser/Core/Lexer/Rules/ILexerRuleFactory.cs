﻿namespace Plastic.Parser.Core.Lexer.Rules
{
	internal interface ILexerRuleFactory<T>
	{
		IConfigurableLexerRule<T> Create(T tokentType);
	}
}