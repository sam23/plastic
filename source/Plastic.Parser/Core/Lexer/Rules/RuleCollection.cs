﻿using System.Collections.Generic;
using System.Linq;

namespace Plastic.Parser.Core.Lexer.Rules
{
	internal class RuleCollection<T>
	{
		private readonly List<ILexerRule<T>> _lexerRuleList;

		public RuleCollection(IEnumerable<ILexerRule<T>> rules)
		{
			_lexerRuleList = rules.ToList();
		}

		public int Count
		{
			get { return _lexerRuleList.Count; }
		}

		public IEnumerable<IRuleResult<T>> Accept(ILexerStateInternal state)
		{
			return _lexerRuleList.Select(x => x.Accept(state)).Where(x => x.Success);
		}
	}
}