namespace Plastic.Parser.Core.Lexer.Rules
{
	internal class VerificationResult : IVerificationResult
	{
		public bool Success { get; set; }
		public bool Completed { get; set; }
	}
}