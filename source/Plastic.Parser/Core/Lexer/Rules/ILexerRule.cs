﻿namespace Plastic.Parser.Core.Lexer.Rules
{
	internal interface ILexerRule<T>
	{
		T TokenType { get; }
		IRuleResult<T> Accept(ILexerStateInternal state);
	}
}