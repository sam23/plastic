﻿using System;
using Plastic.Parser.Lexer.Verification;

namespace Plastic.Parser.Core.Lexer.Rules
{
	internal class VerificationChain
	{
		private readonly Guid _id;
		private readonly IVerification _verification;
		private VerificationChain _nextVerification;

		public VerificationChain(IVerification verification, Guid id)
		{
			_verification = verification;
			_id = id;
		}

		public IVerificationResult Verify(ILexerStateInternal state)
		{
			return state.IsSuccessfulVerification(_id) ? VerifyNext(state) : VerifyInternal(state);
		}

		public void AddSuccessor(VerificationChain verificationChain)
		{
			if (_nextVerification != null)
			{
				_nextVerification.AddSuccessor(verificationChain);
			}
			else
			{
				_nextVerification = verificationChain;
			}
		}

		public override string ToString()
		{
			return string.Format("[Id={0}, Type={1}]{2}", _id, _verification, _nextVerification != null ? string.Format(", {0}", _nextVerification) : string.Empty);
		}

		private IVerificationResult VerifyNext(ILexerStateInternal state)
		{
			if (_nextVerification == null)
			{
				return new VerificationResult { Success = false, Completed = true };
			}

			return _nextVerification.Verify(state);
		}

		private IVerificationResult VerifyInternal(ILexerStateInternal state)
		{
			var verifcationSuccess = _verification.Verify(state);

			if (!verifcationSuccess && state.Position > 0 && (_verification.Occurence == Occurence.OneOrMany))
			{
				state.AddSuccessfulVerification(_id);

				return VerifyNext(state);
			}

			if (!verifcationSuccess && state.Position == 0 && (_verification.Occurence == Occurence.ZeroOrSingle || _verification.Occurence == Occurence.ZeroOrMany))
			{
				state.AddSuccessfulVerification(_id);

				return VerifyNext(state);
			}

			if (verifcationSuccess && (_verification.Occurence == Occurence.ZeroOrSingle || _verification.Occurence == Occurence.Single))
			{
				state.AddSuccessfulVerification(_id);
			}

			return new VerificationResult { Success = verifcationSuccess, Completed = IsCompleted };
		}

		private bool IsCompleted
		{
			get { return _nextVerification == null || _nextVerification.IsOptional; }
		}

		private bool IsOptional
		{
			get { return (_verification.Occurence == Occurence.ZeroOrSingle || _verification.Occurence == Occurence.ZeroOrMany) && IsCompleted; }
		}
	}
}