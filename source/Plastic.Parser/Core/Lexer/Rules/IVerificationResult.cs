﻿namespace Plastic.Parser.Core.Lexer.Rules
{
	internal interface IVerificationResult
	{
		bool Success { get; }
		bool Completed { get; }
	}
}