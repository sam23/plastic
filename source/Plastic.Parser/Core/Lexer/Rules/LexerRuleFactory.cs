namespace Plastic.Parser.Core.Lexer.Rules
{
	internal class LexerRuleFactory<T> : ILexerRuleFactory<T>
	{
		private readonly IIdentifierGenerator _identifierGenerator;

		public LexerRuleFactory(IIdentifierGenerator identifierGenerator)
		{
			_identifierGenerator = identifierGenerator;
		}

		public IConfigurableLexerRule<T> Create(T tokenType)
		{
			return new GenericLexerRule<T>(tokenType, _identifierGenerator);
		}
	}
}