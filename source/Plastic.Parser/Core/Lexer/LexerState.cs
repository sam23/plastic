﻿using System;
using System.Collections.Generic;

namespace Plastic.Parser.Core.Lexer
{
	internal class LexerState : ILexerStateInternal
	{
		private readonly List<char> _characterBuffer = new List<char>();
		private readonly HashSet<Guid> _successfulVerifications = new HashSet<Guid>();
		private char _currentCharacter = Char.MinValue;

		public char LookAhead
		{
			get { return _currentCharacter; }
			set
			{
				if (_currentCharacter != Char.MinValue)
				{
					_characterBuffer.Add(_currentCharacter);
				}
				_currentCharacter = value;
			}
		}

		public int Position
		{
			get { return _characterBuffer.Count; }
		}

		public void AddSuccessfulVerification(Guid id)
		{
			_successfulVerifications.Add(id);
		}

		public bool IsSuccessfulVerification(Guid id)
		{
			return _successfulVerifications.Contains(id);
		}

		public void Reset()
		{
			_characterBuffer.Clear();
			_successfulVerifications.Clear();
		}

		public override string ToString()
		{
			return new string(_characterBuffer.ToArray());
		}
	}
}