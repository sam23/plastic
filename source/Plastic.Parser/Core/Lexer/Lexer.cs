﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plastic.Parser.Core.Lexer.Rules;
using Plastic.Parser.Core.Stream;
using Plastic.Parser.Lexer;

namespace Plastic.Parser.Core.Lexer
{
	internal class Lexer<T> : ILexer<T>
	{
		private readonly IInputStream _inputStream;
		private readonly LexerState _lexerState = new LexerState();
		private readonly RuleCollection<T> _lexerRules;

		public Lexer(IInputStream inputStream, IEnumerable<ILexerRule<T>> lexerRules)
		{
			_inputStream = inputStream;

			_lexerRules = new RuleCollection<T>(lexerRules);
		}

		public Token<T> GetNextToken()
		{
			if (_lexerRules.Count == 0)
			{
				throw new InvalidOperationException("No grammar found.");
			}

			List<IRuleResult<T>> bufferedResults = null;

			while (true)
			{
				_lexerState.LookAhead = _inputStream.LookAhead(1);

				var results = GetRuleResults(GetCurrentRules(bufferedResults)).ToList();

				if (results.Count == 0)
				{
					if (bufferedResults != null)
					{
						var completedResults = bufferedResults.Where(x => x.Completed).ToList();
						if (completedResults.Count == 1)
						{
							return CreateTokenFromResult(completedResults[0]);
						}
					}

					if (_lexerState.LookAhead == Char.MinValue)
					{
						throw new Exception(string.Format("Unregnognized token '{0}'.", _lexerState));
					}
				}

				bufferedResults = results;

				_inputStream.Consume();
			}
		}

		private Token<T> CreateTokenFromResult(IRuleResult<T> result)
		{
			var token = new Token<T>(result.Rule.TokenType, _lexerState.ToString());

			_lexerState.Reset();

			return token;
		}

		private IEnumerable<IRuleResult<T>> GetRuleResults(RuleCollection<T> ruleCollection)
		{
			return ruleCollection.Accept(_lexerState);
		}

		private RuleCollection<T> GetCurrentRules(IEnumerable<IRuleResult<T>> results)
		{
			return results != null ? new RuleCollection<T>(results.Select(x => x.Rule)) : _lexerRules;
		}
	}
}