﻿using System;

namespace Plastic.Parser.Core
{
	internal class IdentifierGenerator : IIdentifierGenerator
	{
		public Guid Generate()
		{
			return Guid.NewGuid();
		}
	}
}