﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Plastic.Parser.Core.Stream;
using Plastic.Parser.Lexer;
using Plastic.Parser.Tests.Simple;

namespace Plastic.Parser.Tests.Core.Lexer
{
	[TestClass]
	public class LexerTests
	{
		[TestMethod]
		public void LexerPlusToken()
		{
			var lexer = CreateSimpleLexer("+");

			var token = lexer.GetNextToken();

			Assert.AreEqual(SimpleTokens.Plus, token.TokenType);
			Assert.AreEqual("+", token.Data);
		}

		[TestMethod]
		public void LexerLowerEqualToken()
		{
			var lexer = CreateSimpleLexer("<=");

			var token = lexer.GetNextToken();

			Assert.AreEqual(SimpleTokens.LowerEquals, token.TokenType);
			Assert.AreEqual("<=", token.Data);
		}

		[TestMethod]
		public void LexerSingleCharacterNumberToken()
		{
			var lexer = CreateSimpleLexer("1");

			var token = lexer.GetNextToken();

			Assert.AreEqual(SimpleTokens.Integer, token.TokenType);
			Assert.AreEqual("1", token.Data);
		}

		[TestMethod]
		public void LexerMultiCharacterNumberToken()
		{
			var lexer = CreateSimpleLexer("123");

			var token = lexer.GetNextToken();

			Assert.AreEqual(SimpleTokens.Integer, token.TokenType);
			Assert.AreEqual("123", token.Data);
		}

		[TestMethod]
		public void LexerMultiCharacterFloatNumberToken()
		{
			var lexer = CreateSimpleLexer("1.4");

			var token = lexer.GetNextToken();

			Assert.AreEqual(SimpleTokens.Float, token.TokenType);
			Assert.AreEqual("1.4", token.Data);
		}

		[TestMethod]
		[ExpectedException(typeof(Exception))]
		public void LexerHandleEmptyString()
		{
			var lexer = CreateSimpleLexer(string.Empty);

			lexer.GetNextToken();
		}

		[TestMethod]
		[ExpectedException(typeof(Exception))]
		public void LexerUnrecognizedToken()
		{
			var lexer = CreateSimpleLexer("|");

			lexer.GetNextToken();
		}

		private static ILexer<SimpleTokens> CreateSimpleLexer(string text)
		{
			return CreateLexer(text, new SimpleGrammar());
		} 

		private static ILexer<T> CreateLexer<T>(string text, IGrammar<T,SimpleExpressions> grammar)
		{
			return grammar.CreateLexer(new CharacterStream(text));
		}
	}
}
