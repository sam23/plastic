﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Plastic.Parser.Core.Stream;

namespace Plastic.Parser.Tests.Core.Stream
{
	[TestClass]
	public class CharacterStreamTests
	{
		[TestMethod]
		public void ConsumeSingleCharacter()
		{
			var characterStream = new CharacterStream("1");

			var result = characterStream.Consume();

			Assert.AreEqual('1', result);
			Assert.AreEqual(1, characterStream.Position);

			result = characterStream.Consume();

			Assert.AreEqual(Char.MinValue, result);
			Assert.AreEqual(2, characterStream.Position);
		}

		[TestMethod]
		public void ConsumeCharacters()
		{
			var characterStream = new CharacterStream("12");

			var result = characterStream.Consume();
			Assert.AreEqual(1, characterStream.Position);

			Assert.AreEqual('1', result);

			result = characterStream.Consume();

			Assert.AreEqual('2', result);
			Assert.AreEqual(2, characterStream.Position);
		}

		[TestMethod]
		public void TakeCharacterOfEmtpyString()
		{
			var characterStream = new CharacterStream(string.Empty);

			var result = characterStream.Consume();

			Assert.AreEqual(Char.MinValue, result);
			Assert.AreEqual(1, characterStream.Position);
		}

		[TestMethod]
		public void LookAheadNextCharacter()
		{
			var characterStream = new CharacterStream("12");

			var result = characterStream.LookAhead(1);

			Assert.AreEqual('1', result);
			Assert.AreEqual(0, characterStream.Position);
		}

		[TestMethod]
		public void LookAheadNextNextCharacter()
		{
			var characterStream = new CharacterStream("12");

			var result = characterStream.LookAhead(2);

			Assert.AreEqual('2', result);
			Assert.AreEqual(0, characterStream.Position);
		}
	}
}