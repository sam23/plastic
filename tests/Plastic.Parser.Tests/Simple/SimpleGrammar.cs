﻿namespace Plastic.Parser.Tests.Simple
{
	public class SimpleGrammar : AbstractGrammar<SimpleTokens, SimpleExpressions>
	{
		public SimpleGrammar()
		{
			Expressions();
			Tokens();
		}

		private void Expressions()
		{
			Expression(SimpleExpressions.Value).Accept(SimpleTokens.Integer).Or(SimpleTokens.Float);
		}

		private void Tokens()
		{
			Token(SimpleTokens.Integer)
				.AcceptDigits().OneOrMany();

			Token(SimpleTokens.Float)
				.AcceptDigits().OneOrMany()
				.Accept('.').Single()
				.AcceptDigits().OneOrMany();

			Token(SimpleTokens.Plus).Accept('+').Single();
			Token(SimpleTokens.Minus).Accept('-').Single();
			Token(SimpleTokens.Multiply).Accept('*').Single();
			Token(SimpleTokens.Divide).Accept('/').Single();
			Token(SimpleTokens.Lower).Accept('<').Single();
			Token(SimpleTokens.Greater).Accept('>').Single();
			Token(SimpleTokens.LowerEquals).Is("<=");
			Token(SimpleTokens.GreaterEquals).Is(">=");
			Token(SimpleTokens.Equal).Accept('=').Single();
		}
	}
}