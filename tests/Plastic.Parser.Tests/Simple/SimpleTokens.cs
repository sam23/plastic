﻿namespace Plastic.Parser.Tests.Simple
{
	public enum SimpleTokens
	{
		Integer,
		Float,
		Plus,
		Minus,
		Multiply,
		Divide,
		Lower,
		Greater,
		LowerEquals,
		GreaterEquals,
		Equal
	}
}